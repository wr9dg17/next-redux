import axios from "axios";
import cookies from "cookie";
import { wrapper } from "../redux/store";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setAuth } from "../redux/actions/authActions";

import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";

function MyApp({ Component, pageProps, auth }) {
  const dispatch = useDispatch();

  console.log(auth);

  // useEffect(() => {
  //   dispatch(setAuth({ user: auth.user, token: auth.access_token }));
  // });

  return <Component {...pageProps} />;
}

MyApp.getInitialProps = async ({ ctx }) => {
  if (ctx.req) {
    const token = cookies.parse(ctx.req.headers.cookie || "");

    if (token["auth_token"]) {
      const { data: res } = await axios.get(`${process.env.API_BASE_URL}/auth/details`, {
        headers: {
          Authorization: `Bearer ${token["auth_token"]}`
        }
      });
      return {
        auth: {
          user: res.data,
          access_token: token["auth_token"]
        }
      }
    }
  }
}

export default wrapper.withRedux(MyApp);
