import { useState } from "react";
import { useDispatch } from "react-redux";
import { login, getDetails } from "../../redux/actions/authActions";
import { Container, Row, Col, Form, Button } from "react-bootstrap";

export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();

    const handleLogin = async (e) => {
        e.preventDefault();
        dispatch(login({ email, password }))
            .then(() => {
                dispatch(getDetails());
            })
    }

    return (
        <Container>
            <Row>
                <Col>
                    <h1 className="my-5">Login</h1>
                    <Form onSubmit={(e) => handleLogin(e)}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}