import axios from "axios";
import cookies from "js-cookie";

const API = axios.create({
    baseURL: process.env.API_BASE_URL
});

API.interceptors.request.use(function(config) {
    const token = cookies.get("auth_token");
    config.headers["Authorization"] = token ? `Bearer ${token}` : "";
    return config;
});

export default API;
