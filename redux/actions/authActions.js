import axiosInstance from "../../config/axios";

import * as types from "../types";

export const login = (formData) => async (dispatch) => {
    const { data: res } = await axiosInstance.post("/auth/login", formData);
    dispatch({ type: types.LOGIN, payload: res.data });
}

export const logout = () => async (dispatch) => {
    await axiosInstance.post("/auth/logout");
    dispatch({ type: types.LOGOUT });
}

export const getDetails = () => async (dispatch) => {
    const { data: res } = await axiosInstance.get("/auth/details");
    dispatch({ type: types.GET_DETAILS, payload: res.data });
}

export const setAuth = ({ user, token }) => (dispatch) => {
    dispatch({ type: types.SET_AUTH, payload: { user, token } });
}
