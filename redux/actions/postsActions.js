import * as types from "../types";

export const fetchPosts = () => async (dispatch) => {
    dispatch({
        type: types.GET_POSTS,
        payload: ["Post 1", "Post 2", "Post 3"]
    })
}