import { HYDRATE } from "next-redux-wrapper";
import * as types from "../types";

const initialState = {
    posts: [],
    post: {},
    loading: false
};

const postsReducer = (state = initialState, action) => {
    switch(action.type) {
        case HYDRATE:
            return state;
        case types.GET_POSTS:
            return {
                ...state,
                loading: false,
                posts: action.payload,
            }
        default:
            return state;
    }
}

export default postsReducer;