import cookies from "js-cookie";
import { HYDRATE } from "next-redux-wrapper";
import * as types from "../types";

const initialState = {
    user: {},
    access_token: ""
};

const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case HYDRATE:
            return state;
        case types.LOGIN:
            cookies.set("auth_token", action.payload.access_token);
            return {
                ...state,
                access_token: action.payload.access_token
            }
        case types.LOGOUT:
            cookies.remove("auth_token");
            return {
                ...state,
                user: {},
                access_token: ""
            }
        case types.GET_DETAILS:
            return {
                ...state,
                user: action.payload
            }
        case types.SET_AUTH:
            return {
                ...state,
                user: action.payload.user,
                access_token: action.payload.token
            }
        default:
            return state;
    }
}

export default authReducer;