export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const GET_DETAILS = "GET_DETAILS";
export const SET_AUTH = "SET_AUTH";
export const GET_POSTS = "GET_POSTS";