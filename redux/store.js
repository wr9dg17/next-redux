import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createWrapper } from "next-redux-wrapper";
import thunk from "redux-thunk";

import rootReducer from "./reducers";

const bindMiddlewares = (middlewares) => {
    if (process.env.NODE_ENV !== "production") {
        return composeWithDevTools(applyMiddleware(...middlewares));
    }
    return applyMiddleware(...middlewares);
}

const initStore = () => {
   return createStore(rootReducer, bindMiddlewares([thunk]));
}

export const wrapper = createWrapper(initStore);